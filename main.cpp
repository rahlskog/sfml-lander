#include <cmath>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

constexpr float G = 3.f;
constexpr float viscosity = 0.25f;

class Lander
{
    const float vertical_thrust = 2.0f;
    const float lateral_thrust = 1.5f;

    sf::Texture texture;
    sf::Sprite sprite;
    sf::Vector2f velocity;
    bool dead;
public:
    Lander(std::string texturefile)
    {
        texture.loadFromFile("lander.png");
        sprite.setTexture(texture);
        velocity = {0.0f, 0.0f};
    }

    sf::Sprite& getSprite() {
        return sprite;
    }

    void reset(float x)
    {
        dead = false;
        sprite.setColor(sf::Color::White);
        sprite.setPosition(x, 0.0f);
        velocity = {0.0f, 0.0f};
    }

    void kill()
    {
        dead = true;
        if (velocity.y > 0.6f*G) {
            sprite.setColor(sf::Color::Red);
        } else {
            sprite.setColor(sf::Color::Green);
        }

    }

    sf::RectangleShape getVextorY()
    {
        sf::RectangleShape vector({2.f, 10*velocity.y});
        if (fabs(velocity.y) > 0.6f*G) {
            vector.setFillColor(sf::Color::Red);
        } else if (fabs(velocity.y) > 0.4f*G) {
            vector.setFillColor(sf::Color::Yellow); 
        } else {
            vector.setFillColor(sf::Color::Green);
        }
        return vector;
    }

    sf::RectangleShape getVextorX()
    {
        sf::RectangleShape vector({5*velocity.x, 2.f});
        return vector;
    }

    void update(sf::Time step)
    {
        if (dead) {
            return;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            velocity.y -= step.asSeconds() * G * vertical_thrust;
        } else {
            velocity.y += step.asSeconds() * G;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            velocity.x -= step.asSeconds() * G * lateral_thrust;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            velocity.x += step.asSeconds() * G * lateral_thrust;
        } else {
            velocity.x *= 1 - (step.asSeconds() * viscosity);
        }
        
        sprite.move(velocity);
    }
};

class Overlay
{
    sf::Font font;
    sf::Text text;
public:
    Overlay()
    {
        font.loadFromFile("OpenSans-Regular.ttf");
        text.setFont(font);
        text.setCharacterSize(24);
        text.setFillColor(sf::Color::Red);
    }

    void update(sf::Time step)
    {
        text.setString(std::to_string(step.asMilliseconds()) + "ms");
    }

    sf::Text& getText()
    {
        return text;
    }
};


int main()
{
    sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Lander");
    window.setVerticalSyncEnabled(true);

    sf::Clock clock;
    Overlay overlay;
    Lander lander("lander.png");
    lander.reset(50.f);


    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed) {
                window.close();
            } else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Key::Space) {
                lander.reset(window.getSize().x/2.0f);
            }
        }

        sf::Time elapsed = clock.restart();
        overlay.update(elapsed);
        lander.update(elapsed);
        if (lander.getSprite().getPosition().y + 48 > window.getSize().y) {
            lander.kill();
        }

        window.clear(sf::Color::Black);
        
        window.draw(lander.getSprite());

        auto xv = lander.getVextorX();
        auto yv = lander.getVextorY();
        xv.setPosition(999, 25);
        yv.setPosition(999, 25);
        window.draw(xv);
        window.draw(yv);
        window.draw(overlay.getText());
        window.display();
    }

    return 0;
}
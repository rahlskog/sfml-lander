## SFML Lander game
Small lander game in C++ / SFML

### Build
Normal cmake, you want a reasonably up to date C++ compiler and sfml dev libraries

    mdkir build
    cd build
    cmake ..
    make

It loads the graphics and font from working directory so either copy them into build or run it from the project root like

    ./build/lander

### Controls
Arrow keys to control movement, space resets the game to start conditions.

There is a velocity indicator top left try to land with it in the green.

![screenshot](/screenshot.png)